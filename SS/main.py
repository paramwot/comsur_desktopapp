from PyQt5.QtWidgets import QWidget, QApplication,QMainWindow
import sys
from main_window import Ui_MainWindow
import pyautogui
import time
import os
import threading


class ScreenShot(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setFixedWidth(640)
        self.setFixedHeight(480)
        self.start_button.clicked.connect(self.start_thread)
        self.stop_button.clicked.connect(self.stop_capture)
        self.capture_flag = False
        self.t1 = None
        self.show()

    def start_thread(self):
        self.t1 = threading.Thread(target=self.start_capture)
        self.t1.start()

    def start_capture(self):
        images_path = "./screenshot/"
        self.capture_flag = True
        if not os.path.exists(images_path):
            os.mkdir(images_path)
        while self.capture_flag:
            print("started")
            image = pyautogui.screenshot()
            ts = time.time()
            fname = "screenshot"
            fname += str(ts)
            fname += ".jpg"
            image.save(images_path + fname)
            time.sleep(2)

    def stop_capture(self):
        print("stop")
        self.capture_flag = False
        if self.t1.is_alive():
            self.t1.join()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    screen_shot = ScreenShot()
    sys.exit(app.exec_())
